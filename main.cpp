#include <cstdlib>
#include <iostream>
#include <windows.h>

/*
Primer par�metro: Nomobre del archivo (implicito al ejecutar).
Segundo par�metro: Intervalo de chequeo en milisegundos.
Tercer par�metro: Directorio.
Cuarto par�metro: Nombre de archivo a buscar.
Quinto par�metro: Timeout en milisegundos
*/

using namespace std;

int main(int argc, char *argv[])
{
    int i;
    int cod_salida; //variable para guardar el c�digo de salida
    int intervalo = atoi(argv[1]); // asigna el intervalo de sondeo (string to int)
    int timeout = atoi(argv[4]); // asigna el timeout (string to int)
    int ciclos = timeout / intervalo;
            
    char tmp[128];
    char texto[][32] = {"Nombre del archivo", "Intervalo de sondeo en ms", "Carpeta de trabajo", "Extension o nombre a buscar", "Timeout en ms"};

    HANDLE hFind; //Handle del buscador de archivos
    WIN32_FIND_DATA FindFileData; //Estructura en donde se guarda los datos de los archivos a procesar
    DWORD tiempo; //variable utilizada para el control del bucle por timeout
    HWND buscador; //Handle de la ventana del programa

    //cout << "Valor argc: " << argc << endl;
    if (argc != 5){
        cout << "El numero de parametros ingresados no es correcto, se ingresaron " << argc << " parametros:" << endl;
        for(i=0; i<argc; i++){
                 cout << i+1 << ") " << texto[i] << " : " << argv[i] << endl;
        }
        cout << "Se requieren los siguientes parametros:" << endl;
        for(; i<5; i++){
                 cout << i+1 << ") " << texto[i] << endl;
        }
        cod_salida = 87; //se le asigna el codigo de error 87 que corresponde a error en los par�metros
        cout << "El codigo de salida es: " << cod_salida << endl;
        exit(cod_salida);
    }
    cout << "Archivo a buscar: " << argv[2] << "\\" << argv[3] << endl;
    if(ciclos <= 0){
        cout << "Error al calcular numeros de ciclos, estableciendo valor por defecto (30 segundos)" << endl;
        ciclos = 30000;
    }
    else
        cout << "Numero de ciclos: " << ciclos << endl;
    tiempo=GetTickCount();//Obtiene la hora actual en milisegundos
    sprintf(tmp,"%s//%s",argv[2],argv[3]);
    for(i=0; i<ciclos; i++){
        hFind = FindFirstFile(tmp, &FindFileData);
        if((hFind != INVALID_HANDLE_VALUE)){
            cout << "Se encontro el archivo en el intento: "  << i << endl;
            cod_salida = 0; //se le asigna el codigo de error 0 que corresponde a finalizacion correcta del programa
            break;
        }
        //else{
        //    cout << "No se encontro el archivo. Intento: "  << i << endl;
        //}
        if( ( GetTickCount()-tiempo ) >= timeout ){
           cout << "Error: se agoto el tiempo de espera" << endl;
           cod_salida = 121; //se le asigna el codigo de error 0 que corresponde al error de timeout
           break;
        }
        Sleep(intervalo);
    }
    if (i>(ciclos-1)){
       cout << "Error: se completo el bucle sin poder encontrar el archivo" << endl;
       cod_salida = 1004; //se le asigna el codigo de error 1004 que corresponde al error de bandera incorrecta
    }
    //system("PAUSE");
    //cout << "El codigo de salida es: " << cod_salida << endl;
    exit(cod_salida);
}
