Buscador de archivos para Windows. 
Compilar con Dev-C++ 4.9.9.2

Funciona con 4 parámetros: 
1) Intervalo de sondeo en ms
2) Ruta
3) Extensión / Nombre de archivo a buscar
4) Timeout

Por ejemplo con un intervalo de sondeo de 100 ms, timeout de 10 segundos, buscando archivos PDF en la carpeta C:\Archivos
100 C:\Archivos *.pdf 10000
